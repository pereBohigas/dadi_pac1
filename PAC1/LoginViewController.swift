//
//  LoginViewController.swift
//  PR1
//
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var welcomeLabel: UILabel!

    // Strings for the error message
    let errorLoginMessageTitle: String = "Login error"
    let errorLoginMessageContent: String = "Sorry, the username and password are invalid"
    
    // BEGIN-UOC-2
    @IBAction func loginTapped(_ sender: UIButton) {
        // Optional chaining to unwrap the text values of the UITextFields to prevent reference errors
        if let enteredUsername: String = usernameField?.text, let enteredPassword: String = passwordField?.text {
            // Use utils' function to validate login credentials
            let loginAllowed = Services.validate(username: enteredUsername, password: enteredPassword)
            if loginAllowed {
                print("Login screen: login succeed")
                performSegue(withIdentifier: "SegueToAuthentication", sender: self)
            } else {
                print("Login screen: login error")
                Utils.show(Message: errorLoginMessageContent, WithTitle: errorLoginMessageTitle, InViewController: self)
            }
        }
        
    }
    // END-UOC-2
    
    // BEGIN-UOC-5
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Clear UITextFields from previous content
        usernameField.text = ""
        passwordField.text = ""
    }
    // END-UOC-5
    
    // BEGIN-UOC-6
    override func viewDidLoad() {
        super.viewDidLoad()
        setPersonalizedScreen()
    }
    
    func setPersonalizedScreen() {
        // Get current date and calendar objects
        let date = Date()
        let calendar = Calendar.current
        // Extract current hour from current date
        let currentHour = calendar.component(.hour, from: date)
        // Set background color and title depending on time
        switch currentHour {
            case 0...5:
                self.view.backgroundColor = UIColor.blue.lighter()
                self.welcomeLabel.text = "Good night"
            case 6...11:
                self.view.backgroundColor = UIColor.orange.lighter()
                self.welcomeLabel.text = "Good morning"
            case 12...17:
                self.view.backgroundColor = UIColor.yellow.lighter()
                self.welcomeLabel.text = "Good afternoon"
            case 18...21:
                self.view.backgroundColor = UIColor.red.lighter()
                self.welcomeLabel.text = "Good evening"
            case 22...24:
                self.view.backgroundColor = UIColor.blue.lighter()
                self.welcomeLabel.text = "Good night"
            default:
                self.view.backgroundColor = UIColor.white.lighter()
                self.welcomeLabel.text = "Welcome"
        }
    }
    // END-UOC-6
    
    @IBAction func unwindToLogin(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        print("Unwind to login")
    }
}

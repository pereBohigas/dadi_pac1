//
//  CalculatorViewController.swift
//  PR1
//
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
    // BEGIN-UOC-7.1
    @IBOutlet weak var initialAmountLabel: UILabel!
    @IBOutlet weak var interestRateLabel: UILabel!
    @IBOutlet weak var yearsLabel: UILabel!
    @IBOutlet weak var finalAmountLabel: UILabel!
    
    @IBOutlet weak var initialAmountSlider: UISlider!
    @IBOutlet weak var interestRateStepper: UIStepper!
    @IBOutlet weak var yearsStepper: UIStepper!
    // END-UOC-7.1
    
    // BEGIN-UOC-7.2
    // Variables for the controls (initializated with initial values)
    var initialAmount: Float = 100
    var interestRate: Double = 0.25
    var years: Double = 1
    
    // CHANGE functions to react to a control's modification
    @IBAction func changeInitialAmount(_ sender: UISlider) {
        initialAmount = initialAmountSlider.value
        setInitialAmountLabel()
        updateFinalAmount()
    }
    
    @IBAction func changeInterestRate(_ sender: UIStepper) {
        interestRate = interestRateStepper.value
        setInterestRateLabel()
        updateFinalAmount()
    }
    
    @IBAction func changeYears(_ sender: UIStepper) {
        years = yearsStepper.value
        setYearsLabel()
        updateFinalAmount()
    }
    
    // UPDATE function to calculte and set the final amount's value
    func updateFinalAmount() {
        let finalAmount = Services.calculateFinalAmount(ForAmount: initialAmount, WithInterest: interestRate / 100, AndYears: years)
        let finalAmountString: String = String(format: "Final amount: %.2f €", finalAmount)
        finalAmountLabel.text = finalAmountString
    }
    
    // SET functions to set the new values into its labels
    func setInitialAmountLabel() {
        let initialAmountString: String = String(format: "Initial amount: %.0f €", initialAmount)
        initialAmountLabel.text = initialAmountString
    }
    
    func setInterestRateLabel() {
        let interestRateString: String = String(format: "Interest rate: %.2f %%", interestRate)
        interestRateLabel.text = interestRateString
    }
    
    func setYearsLabel() {
        let yearsString: String = String(format: "Years: %.0f", years)
        yearsLabel.text = yearsString
    }
    // END-UOC-7.2
    
    // BEGIN-UOC-7.3
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set initial values in its labels and update final amount
        setInitialAmountLabel()
        setInterestRateLabel()
        setYearsLabel()
        updateFinalAmount()
    }
    // END-UOC-7.3
}
